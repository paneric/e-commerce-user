<?php

declare(strict_types=1);

namespace ECommerce\User\Repository;

use Paneric\RelationModule\Interfaces\Repository\ModuleRepositoryInterface;

interface UserRepositoryInterface extends ModuleRepositoryInterface
{}
