<?php

declare(strict_types=1);

namespace ECommerce\User\Repository;

use Paneric\RelationModule\Module\Repository\ModuleRepository;

class UserRepository extends ModuleRepository implements UserRepositoryInterface
{}
