<?php

declare(strict_types=1);

namespace ECommerce\User\Gateway;

use Paneric\DataObject\DAO;

class UserDAO extends DAO
{
    protected $id;
    protected $credentialId; 

    protected $ref;//String 
    protected $isActive;//Int 

    public function __construct()
    {
        $this->prefix = 'usr_';

        $this->setMaps();
    }

    /**
     * @return null|int|string
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return null|int|string
     */
    public function getCredentialId()
    {
        return $this->credentialId;
    } 

    public function getRef(): ?String
    {
        return $this->ref;
    }
    /**
     * @return null|int|string
     */
    public function getIsActive()
    {
        return $this->isActive;
    } 

    /**
     * @var int|string
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    /**
     * @var int|string
     */
    public function setCredentialId($credentialId): void
    {
        $this->credentialId = $credentialId;
    } 

    public function setRef(String $ref): void
    {
        $this->ref = $ref;
    }
    /**
     * @var int|string
     */
    public function setIsActive($isActive): void
    {
        $this->isActive = $isActive;
    } 

}
