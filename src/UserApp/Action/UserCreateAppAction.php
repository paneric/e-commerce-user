<?php

declare(strict_types=1);

namespace ECommerce\User\UserApp\Action;

use Paneric\RelationModule\Module\Action\App\CreateAppAction;
use ECommerce\User\UserApp\Interfaces\Action\UserCreateAppActionInterface;

class UserCreateAppAction
    extends CreateAppAction
    implements UserCreateAppActionInterface
{}
