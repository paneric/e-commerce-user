<?php

declare(strict_types=1);

namespace ECommerce\User\UserApp\Action;

use Paneric\RelationModule\Module\Action\App\AlterAppAction;
use ECommerce\User\UserApp\Interfaces\Action\UserAlterAppActionInterface;

class UserAlterAppAction
    extends AlterAppAction
    implements UserAlterAppActionInterface
{}
