<?php

declare(strict_types=1);

namespace ECommerce\User\UserApp\Action;

use Paneric\RelationModule\Module\Action\App\DeleteAppAction;
use ECommerce\User\UserApp\Interfaces\Action\UserDeleteAppActionInterface;

class UserDeleteAppAction
    extends DeleteAppAction
    implements UserDeleteAppActionInterface
{}
