<?php

declare(strict_types=1);

namespace ECommerce\User\UserApp\Action;

use Paneric\RelationModule\Module\Action\App\GetAllPaginatedAppAction;
use ECommerce\User\UserApp\Interfaces\Action\UserGetAllPaginatedAppActionInterface;

class UserGetAllPaginatedAppAction
    extends GetAllPaginatedAppAction
    implements UserGetAllPaginatedAppActionInterface
{}
