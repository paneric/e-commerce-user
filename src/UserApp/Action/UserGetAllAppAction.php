<?php

declare(strict_types=1);

namespace ECommerce\User\UserApp\Action;

use Paneric\RelationModule\Module\Action\App\GetAllAppAction;
use ECommerce\User\UserApp\Interfaces\Action\UserGetAllAppActionInterface;

class UserGetAllAppAction
    extends GetAllAppAction
    implements UserGetAllAppActionInterface
{}
