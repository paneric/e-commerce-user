<?php

declare(strict_types=1);

namespace ECommerce\User\UserApp\Action;

use Paneric\RelationModule\Module\Action\App\GetOneByIdAppAction;
use ECommerce\User\UserApp\Interfaces\Action\UserGetOneByIdAppActionInterface;

class UserGetOneByIdAppAction
    extends GetOneByIdAppAction
    implements UserGetOneByIdAppActionInterface
{}
