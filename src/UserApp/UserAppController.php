<?php

declare(strict_types=1);

namespace ECommerce\User\UserApp;

use Paneric\RelationModule\Module\Controller\ModuleAppController;

class UserAppController extends ModuleAppController
{}
