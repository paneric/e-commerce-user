<?php

declare(strict_types=1);

namespace ECommerce\User\UserApp\Interfaces\Action;

use Paneric\RelationModule\Interfaces\Action\App\CreateAppActionInterface;

interface UserCreateAppActionInterface extends CreateAppActionInterface
{}
