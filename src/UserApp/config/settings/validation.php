<?php

declare(strict_types=1);

use ECommerce\User\Gateway\UserDTO;

return [

    'validation' => [

        'usr.add' => [
            'methods' => ['POST'],
            UserDTO::class => [
                'rules' => [
                    'credential_id' => [
                        'required' => []
                    ], 

                    'ref' => [
                        'required' => []
                    ], 
                    'is_active' => [
                        'required' => []
                    ], 

                ],
            ],
        ],

        'usr.edit' => [
            'methods' => ['POST'],
            UserDTO::class => [
                'rules' => [
                    'credential_id' => [
                        'required' => []
                    ], 

                    'ref' => [
                        'required' => []
                    ], 
                    'is_active' => [
                        'required' => []
                    ], 

                ],
            ],
        ],

        'usr.remove' => [
            'methods' => ['POST'],
            UserDTO::class => [
                'rules' => [],
            ],
        ],
    ]
];
