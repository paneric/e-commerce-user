<?php

declare(strict_types=1);

use ECommerce\Credential\CredentialApp\Action\CredentialGetAllAppAction; 

use ECommerce\User\Gateway\UserDAO;
use ECommerce\User\Gateway\UserDTO;

use Paneric\DIContainer\DIContainer as Container;
use Paneric\Interfaces\Session\SessionInterface;
use Paneric\Validation\ValidationService;

use ECommerce\User\UserApp\Action\UserAlterAppAction;
use ECommerce\User\UserApp\Action\UserCreateAppAction;
use ECommerce\User\UserApp\Action\UserDeleteAppAction;
use ECommerce\User\UserApp\Action\UserGetAllAppAction;
use ECommerce\User\UserApp\Action\UserGetAllPaginatedAppAction;
use ECommerce\User\UserApp\Action\UserGetOneByIdAppAction;

return [
    'user_alter_action' => static function (Container $container): UserAlterAppAction {
        return new UserAlterAppAction(
            $container->get('user_repository'),
            $container->get(SessionInterface::class),
            $container->get(ValidationService::class),
            [
                'module_name_sc' => 'user',
                'prefix' => 'usr',
                'dao_class' => UserDAO::class,
                'dto_class' => UserDTO::class,
                'update_unique_criteria' => static function (array $attributes, string $id): array
                {
                    return ['usr_id' => (int) $id, 'usr_ref' => $attributes['ref']];
                },
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['usr_id' => (int) $id];
                },
            ]
        );
    },
    'user_create_action' => static function (Container $container): UserCreateAppAction {
        return new UserCreateAppAction(
            $container->get('user_repository'),
            $container->get(SessionInterface::class),
            $container->get(ValidationService::class),
            [
                'module_name_sc' => 'user',
                'prefix' => 'usr',
                'dao_class' => UserDAO::class,
                'dto_class' => UserDTO::class,
                'create_unique_criteria' => static function (array $attributes): array
                {
                    return ['usr_ref' => $attributes['ref']];
                },
            ]
        );
    },
    'user_delete_action' => static function (Container $container): UserDeleteAppAction {
        return new UserDeleteAppAction(
            $container->get('user_repository'),
            $container->get(SessionInterface::class),
            [
                'module_name_sc' => 'user',
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['usr_id' => (int) $id];
                },
            ]
        );
    },
    'user_get_all_paginated_action' => static function (Container $container): UserGetAllPaginatedAppAction {
        return new UserGetAllPaginatedAppAction(
            $container->get('user_repository'),
            $container->get(SessionInterface::class),
            [
                'module_name_sc' => 'user',
                'prefix' => 'usr',
                'find_by_criteria' => static function (): array
                {
                    return [];
                },
                'order_by' => static function (): array
                {
                    return ['usr_ref' => 'ASC'];
                },
            ]
        );
    },
    'user_get_all_action' => static function (Container $container): UserGetAllAppAction {
        return new UserGetAllAppAction(
            $container->get('user_repository'),
            $container->get(SessionInterface::class),
            [
                'module_name_sc' => 'user',
                'prefix' => 'usr',
                'find_by_criteria' => static function (): array
                {
                    return [];
                },
                'order_by' => static function (): array
                {
                    return ['usr_ref' => 'ASC'];
                },
            ]
        );
    },
    'user_get_one_by_id_action' => static function (Container $container): UserGetOneByIdAppAction {
        return new UserGetOneByIdAppAction(
            $container->get('user_repository'),
            $container->get(SessionInterface::class),
            [
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['usr_id' => (int) $id];
                },
            ]
        );
    },


    'credential_get_all_action' => static function (Container $container): CredentialGetAllAppAction {
        return new CredentialGetAllAppAction(
            $container->get('credential_repository'),
            $container->get(SessionInterface::class),
            [
                'module_name_sc' => 'credential',
                'prefix' => 'crd',
                'order_by' => static function (string $local): array
                {
                    return [];
                },
            ]
        );
    }, 

];
