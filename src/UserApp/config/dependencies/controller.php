<?php

declare(strict_types=1);

use Paneric\DIContainer\DIContainer as Container;
use ECommerce\User\UserApp\UserAppController;
use Twig\Environment as Twig;

return [
    'user_controller' => static function (Container $container): UserAppController {
        return new UserAppController(
            $container->get(Twig::class),
            'usr'
        );
    },
];
