<?php

declare(strict_types=1);

use Paneric\Middleware\CSRFMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {
    $app->get('/usr/show-all', function (Request $request, Response $response) {
        return $this->get('user_controller')->showAll(
            $response,
            array_merge(
                [],
                $this->get('credential_get_all_action')->getAll($request), 

            ),
            $this->get('user_get_all_action')
        );
    })->setName('usr.show_all');

    $app->get('/usr/show-one-by-id/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('user_controller')->showOneById(
            $response,
            array_merge(
                [],
                $this->get('credential_get_all_action')->getAll($request), 

            ),
            $this->get('user_get_one_by_id_action'),
            $args['id']
        );
    })->setName('usr.show_one_by_id');

    $app->map(['GET', 'POST'], '/usr/add', function (Request $request, Response $response) {
        return $this->get('user_controller')->add(
            $request,
            $response,
            array_merge(
                [],
                $this->get('credential_get_all_action')->getAll($request), 

            ),
            $this->get('user_create_action')
        );
    })->setName('usr.add')
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/usr/edit/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('user_controller')->edit(
            $request,
            $response,
            array_merge(
                [],
                $this->get('credential_get_all_action')->getAll($request), 

            ),
            $this->get('user_alter_action'),
            $args['id']
        );
    })->setName('usr.edit')
        ->addMiddleware($container->get(CSRFMiddleware::class));

    $app->map(['GET', 'POST'], '/usr/remove/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('user_controller')->remove(
            $request,
            $response,
            $this->get('user_delete_action'),
            $args['id']
        );
    })->setName('usr.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class));
}
