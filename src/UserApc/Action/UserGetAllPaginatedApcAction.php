<?php

declare(strict_types=1);

namespace ECommerce\User\UserApc\Action;

use Paneric\RelationModule\Module\Action\Apc\GetAllPaginatedApcAction;
use ECommerce\User\UserApc\Interfaces\Action\UserGetAllPaginatedApcActionInterface;

class UserGetAllPaginatedApcAction
    extends GetAllPaginatedApcAction
    implements UserGetAllPaginatedApcActionInterface
{}
