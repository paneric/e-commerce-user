<?php

declare(strict_types=1);

namespace ECommerce\User\UserApc\Action;

use Paneric\RelationModule\Module\Action\Apc\DeleteApcAction;
use ECommerce\User\UserApc\Interfaces\Action\UserDeleteApcActionInterface;

class UserDeleteApcAction
    extends DeleteApcAction
    implements UserDeleteApcActionInterface
{}
