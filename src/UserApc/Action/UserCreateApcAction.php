<?php

declare(strict_types=1);

namespace ECommerce\User\UserApc\Action;

use Paneric\RelationModule\Module\Action\Apc\CreateApcAction;
use ECommerce\User\UserApc\Interfaces\Action\UserCreateApcActionInterface;

class UserCreateApcAction
    extends CreateApcAction
    implements UserCreateApcActionInterface
{}
