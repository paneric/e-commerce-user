<?php

declare(strict_types=1);

namespace ECommerce\User\UserApc\Action;

use Paneric\RelationModule\Module\Action\Apc\GetOneByIdApcAction;
use ECommerce\User\UserApc\Interfaces\Action\UserGetOneByIdApcActionInterface;

class UserGetOneByIdApcAction
    extends GetOneByIdApcAction
    implements UserGetOneByIdApcActionInterface
{}
