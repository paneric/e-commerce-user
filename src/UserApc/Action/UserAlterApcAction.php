<?php

declare(strict_types=1);

namespace ECommerce\User\UserApc\Action;

use Paneric\RelationModule\Module\Action\Apc\AlterApcAction;
use ECommerce\User\UserApc\Interfaces\Action\UserAlterApcActionInterface;

class UserAlterApcAction
    extends AlterApcAction
    implements UserAlterApcActionInterface
{}
