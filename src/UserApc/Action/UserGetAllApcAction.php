<?php

declare(strict_types=1);

namespace ECommerce\User\UserApc\Action;

use Paneric\RelationModule\Module\Action\Apc\GetAllApcAction;
use ECommerce\User\UserApc\Interfaces\Action\UserGetAllApcActionInterface;

class UserGetAllApcAction
    extends GetAllApcAction
    implements UserGetAllApcActionInterface
{}
