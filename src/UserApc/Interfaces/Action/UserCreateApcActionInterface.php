<?php

declare(strict_types=1);

namespace ECommerce\User\UserApc\Interfaces\Action;

use Paneric\RelationModule\Interfaces\Action\Apc\CreateApcActionInterface;

interface UserCreateApcActionInterface extends CreateApcActionInterface
{}
