<?php

declare(strict_types=1);

namespace ECommerce\User\UserApc;

use Paneric\RelationModule\Module\Controller\ModuleApcController;

class UserApcController extends ModuleApcController
{}
