<?php

declare(strict_types=1);

use Paneric\DIContainer\DIContainer as Container;
use ECommerce\User\UserApc\UserApcController;
use Twig\Environment as Twig;

return [
    'user_controller' => static function (Container $container): UserApcController {
        return new UserApcController(
            $container->get(Twig::class),
            'apc-usr'
        );
    },
];
