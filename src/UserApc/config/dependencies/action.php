<?php

declare(strict_types=1);

use ECommerce\Credential\CredentialApc\Action\CredentialGetAllApcAction; 

use Paneric\DIContainer\DIContainer as Container;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Interfaces\Session\SessionInterface;
use ECommerce\User\UserApc\Action\UserAlterApcAction;
use ECommerce\User\UserApc\Action\UserCreateApcAction;
use ECommerce\User\UserApc\Action\UserDeleteApcAction;
use ECommerce\User\UserApc\Action\UserGetAllApcAction;
use ECommerce\User\UserApc\Action\UserGetAllPaginatedApcAction;
use ECommerce\User\UserApc\Action\UserGetOneByIdApcAction;

return [
    'user_add_action' => static function (Container $container): UserCreateApcAction {
        return new UserCreateApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => 'user',
                    'prefix' => 'usr'
                ]
            )
        );
    },
    'user_edit_action' => static function (Container $container): UserAlterApcAction {
        return new UserAlterApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => 'user',
                    'prefix' => 'usr'
                ]
            )
        );
    },
    'user_remove_action' => static function (Container $container): UserDeleteApcAction {
        return new UserDeleteApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                ['module_name_sc' => 'user']
            )
        );
    },
    'user_get_all_paginated_action' => static function (Container $container): UserGetAllPaginatedApcAction {
        return new UserGetAllPaginatedApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => 'user',
                    'prefix' => 'usr',
                    'prefixes' => ['usr', 'crds'] // main prefix first !!!
                ]
            )
        );
    },
    'user_get_all_action' => static function (Container $container): UserGetAllApcAction {
        return new UserGetAllApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                [
                    'module_name_sc' => 'user',
                    'prefix' => 'usr',
                    'prefixes' => ['usr', 'crds'] // main prefix first !!!
                ]
            )
        );
    },
    'user_get_one_by_id_action' => static function (Container $container): UserGetOneByIdApcAction {
        return new UserGetOneByIdApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('api_endpoints'),
                ['prefix' => 'usr']
            )

        );
    },

    'credential_get_all_action' => static function (Container $container): CredentialGetAllApcAction {
        return new CredentialGetAllApcAction(
            $container->get(HttpClientManager::class),
            $container->get(SessionInterface::class),
            array_merge(
                $container->get('crd_api_endpoints'),
                ['module_name_sc' => 'credential', 'prefix' => 'crd']
            )
        );
    }, 

];
