<?php

declare(strict_types=1);

use GuzzleHttp\Client;
use Paneric\DIContainer\DIContainer as Container;
use Paneric\HttpClient\HttpClientManager;
use Paneric\Logger\LoggerFactory;

return [
    Client::class => static function(Container $container): Client
    {
        return new Client(
            $container->get('guzzle_client')
        );
    },
    HttpClientManager::class => static function(Container $container): HttpClientManager
    {
        return new HttpClientManager(
            $container->get(Client::class),
            $container->get(LoggerFactory::class),
            [
                'filename' => 'http_client_manager.log',
                'name' => 'http_client_manager',
            ],
        );
    },
];
