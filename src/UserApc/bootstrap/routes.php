<?php

declare(strict_types=1);

use Paneric\Middleware\CSRFMiddleware;
use Paneric\Middleware\JWTAuthenticationEncoderMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {
    $app->get('/apc-usr/show-all', function (Request $request, Response $response, array $args) {
        return $this->get('user_controller')->showAll(
            $request,
            $response,
            array_merge(
                [],
                $this->get('credential_get_all_action')->getAll($request), 

            ),
            $this->get('user_get_all_action')
        );
    })->setName('apc-usr.show_all')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->get('/apc-usr/show-one-by-id/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('user_controller')->showOneById(
            $request,
            $response,
            array_merge(
                [],
                $this->get('credential_get_all_action')->getAll($request), 

            ),
            $this->get('user_get_one_by_id_action'),
            $args['id']
        );
    })->setName('apc-usr.show_one_by_id')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-usr/add', function (Request $request, Response $response) {
        return $this->get('user_controller')->add(
            $request,
            $response,
            array_merge(
                [],
                $this->get('credential_get_all_action')->getAll($request), 

            ),
            $this->get('user_add_action')
        );
    })->setName('apc-usr.add')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-usr/edit/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('user_controller')->edit(
            $request,
            $response,
            array_merge(
                [],
                $this->get('user_get_one_by_id_action')->getOneById($request, $args['id']),
                $this->get('credential_get_all_action')->getAll($request), 

            ),
            $this->get('user_edit_action'),
            $args['id']
        );
    })->setName('apc-usr.edit')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));

    $app->map(['GET', 'POST'], '/apc-usr/remove/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('user_controller')->remove(
            $request,
            $response,
            $this->get('user_remove_action'),
            $args['id']
        );
    })->setName('apc-usr.remove')
        ->addMiddleware($container->get(CSRFMiddleware::class))
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class));
}
