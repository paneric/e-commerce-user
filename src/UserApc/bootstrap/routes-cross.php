<?php

declare(strict_types=1);

use Paneric\Middleware\JWTAuthenticationEncoderMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;

if (isset($app, $container)) {

    $app->get('/apc-usr/show-all-paginated[/{page}]', function (Request $request, Response $response, array $args) {
        return $this->get('user_controller')->showAllPaginated(
            $request,
            $response,
            array_merge(
                [],
                $this->get('credential_get_all_action')->getAll($request), 

            ),
            $this->get('user_get_all_paginated_action'),
            empty($args) ? null : $args['page']
        );
    })->setName('apc-usr.show_all_paginated')
        ->addMiddleware($container->get(JWTAuthenticationEncoderMiddleware::class)); // no pagination middleware !!!
}