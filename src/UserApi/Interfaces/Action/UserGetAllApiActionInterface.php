<?php

declare(strict_types=1);

namespace ECommerce\User\UserApi\Interfaces\Action;

use Paneric\RelationModule\Interfaces\Action\Api\GetAllApiActionInterface;

interface UserGetAllApiActionInterface extends GetAllApiActionInterface
{}
