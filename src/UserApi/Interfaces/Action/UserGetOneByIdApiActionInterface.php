<?php

declare(strict_types=1);

namespace ECommerce\User\UserApi\Interfaces\Action;

use Paneric\RelationModule\Interfaces\Action\Api\GetOneByIdApiActionInterface;

interface UserGetOneByIdApiActionInterface extends GetOneByIdApiActionInterface
{}
