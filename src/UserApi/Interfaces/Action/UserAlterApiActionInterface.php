<?php

declare(strict_types=1);

namespace ECommerce\User\UserApi\Interfaces\Action;

use Paneric\RelationModule\Interfaces\Action\Api\AlterApiActionInterface;

interface UserAlterApiActionInterface extends AlterApiActionInterface
{}
