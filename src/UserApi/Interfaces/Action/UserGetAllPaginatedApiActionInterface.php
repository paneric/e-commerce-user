<?php

declare(strict_types=1);

namespace ECommerce\User\UserApi\Interfaces\Action;

use Paneric\RelationModule\Interfaces\Action\Api\GetAllPaginatedApiActionInterface;

interface UserGetAllPaginatedApiActionInterface extends GetAllPaginatedApiActionInterface
{}
