<?php

declare(strict_types=1);

namespace ECommerce\User\UserApi\Interfaces\Action;

use Paneric\RelationModule\Interfaces\Action\Api\DeleteApiActionInterface;

interface UserDeleteApiActionInterface extends DeleteApiActionInterface
{}
