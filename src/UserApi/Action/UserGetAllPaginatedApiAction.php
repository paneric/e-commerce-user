<?php

declare(strict_types=1);

namespace ECommerce\User\UserApi\Action;

use Paneric\RelationModule\Module\Action\Api\GetAllPaginatedApiAction;
use ECommerce\User\UserApi\Interfaces\Action\UserGetAllPaginatedApiActionInterface;

class UserGetAllPaginatedApiAction
    extends GetAllPaginatedApiAction
    implements UserGetAllPaginatedApiActionInterface
{}
