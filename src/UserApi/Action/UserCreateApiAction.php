<?php

declare(strict_types=1);

namespace ECommerce\User\UserApi\Action;

use Paneric\RelationModule\Module\Action\Api\CreateApiAction;
use ECommerce\User\UserApi\Interfaces\Action\UserCreateApiActionInterface;

class UserCreateApiAction
    extends CreateApiAction
    implements UserCreateApiActionInterface
{}
