<?php

declare(strict_types=1);

namespace ECommerce\User\UserApi\Action;

use Paneric\RelationModule\Module\Action\Api\GetAllApiAction;
use ECommerce\User\UserApi\Interfaces\Action\UserGetAllApiActionInterface;

class UserGetAllApiAction
    extends GetAllApiAction
    implements UserGetAllApiActionInterface
{}
