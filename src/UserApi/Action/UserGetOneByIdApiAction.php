<?php

declare(strict_types=1);

namespace ECommerce\User\UserApi\Action;

use Paneric\RelationModule\Module\Action\Api\GetOneByIdApiAction;
use ECommerce\User\UserApi\Interfaces\Action\UserGetOneByIdApiActionInterface;

class UserGetOneByIdApiAction
    extends GetOneByIdApiAction
    implements UserGetOneByIdApiActionInterface
{}
