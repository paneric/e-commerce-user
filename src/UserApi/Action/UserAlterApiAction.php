<?php

declare(strict_types=1);

namespace ECommerce\User\UserApi\Action;

use Paneric\RelationModule\Module\Action\Api\AlterApiAction;
use ECommerce\User\UserApi\Interfaces\Action\UserAlterApiActionInterface;

class UserAlterApiAction
    extends AlterApiAction
    implements UserAlterApiActionInterface
{}
