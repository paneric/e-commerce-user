<?php

declare(strict_types=1);

namespace ECommerce\User\UserApi\Action;

use Paneric\RelationModule\Module\Action\Api\DeleteApiAction;
use ECommerce\User\UserApi\Interfaces\Action\UserDeleteApiActionInterface;

class UserDeleteApiAction
    extends DeleteApiAction
    implements UserDeleteApiActionInterface
{}
