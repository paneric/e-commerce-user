<?php

declare(strict_types=1);

use Paneric\Pagination\PaginationMiddleware;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Message\ResponseInterface as Response;
use Tuupola\Middleware\JwtAuthentication;

if (isset($app, $container)) {
    $app->get('/api-usrs/{page}', function (Request $request, Response $response, array $args) {
        return $this->get('user_controller')->getAllPaginated(
            $request,
            $response,
            $this->get('user_get_all_paginated_action'),
            $args['page']
        );
    })->setName('get.api-usrs.page')
        ->addMiddleware($container->get(PaginationMiddleware::class))
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->get('/api-usrs', function (Request $request, Response $response, array $args) {
        return $this->get('user_controller')->getAll(
            $request,
            $response,
            $this->get('user_get_all_action')
        );
    })->setName('get.api-usrs')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->get('/api-usr/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('user_controller')->getOneById(
            $response,
            $this->get('user_get_one_by_id_action'),
            $args['id']
        );
    })->setName('get.api-usr')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->post('/api-usr', function (Request $request, Response $response) {
        return $this->get('user_controller')->create(
            $request,
            $response,
            $this->get('user_create_action')
        );
    })->setName('post.api-usr')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->put('/api-usr/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('user_controller')->alter(
            $request,
            $response,
            $this->get('user_alter_action'),
            $args['id']
        );
    })->setName('put.api-usr')
        ->addMiddleware($container->get(JwtAuthentication::class));

    $app->delete('/api-usr/{id}', function (Request $request, Response $response, array $args) {
        return $this->get('user_controller')->delete(
            $request,
            $response,
            $this->get('user_delete_action'),
            $args['id']
        );
    })->setName('delete.api-usr')
        ->addMiddleware($container->get(JwtAuthentication::class));
}
