<?php

declare(strict_types=1);

use ECommerce\User\UserApi\UserApiController;

return [
    'user_controller' => static function (): UserApiController {
        return new UserApiController('usr');
    },
];
