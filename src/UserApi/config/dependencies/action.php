<?php

declare(strict_types=1);

use ECommerce\User\Gateway\UserDAO;
use ECommerce\User\Gateway\UserDTO;

use Paneric\DIContainer\DIContainer as Container;
use Paneric\Validation\ValidationService;

use ECommerce\User\UserApi\Action\UserAlterApiAction;
use ECommerce\User\UserApi\Action\UserCreateApiAction;
use ECommerce\User\UserApi\Action\UserDeleteApiAction;
use ECommerce\User\UserApi\Action\UserGetAllApiAction;
use ECommerce\User\UserApi\Action\UserGetAllPaginatedApiAction;
use ECommerce\User\UserApi\Action\UserGetOneByIdApiAction;

return [
    'user_create_action' => static function (Container $container): UserCreateApiAction {
        return new UserCreateApiAction(
            $container->get('user_repository'),
            $container->get(ValidationService::class),
            [
                'dao_class' => UserDAO::class,
                'dto_class' => UserDTO::class,
                'create_unique_criteria' => static function (array $attributes): array
                {
                    return ['usr_ref' => $attributes['ref']];
                },
            ]
        );
    },
    'user_alter_action' => static function (Container $container): UserAlterApiAction {
        return new UserAlterApiAction(
            $container->get('user_repository'),
            $container->get(ValidationService::class),
            [
                'dao_class' => UserDAO::class,
                'dto_class' => UserDTO::class,
                'update_unique_criteria' => static function (array $attributes, string $id): array
                {
                    return ['usr_id' => (int) $id, 'usr_ref' => $attributes['ref']];
                },
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['usr_id' => (int) $id];
                },
            ]
        );
    },
    'user_delete_action' => static function (Container $container): UserDeleteApiAction {
        return new UserDeleteApiAction(
            $container->get('user_repository'),
            [
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['usr_id' => (int) $id];
                },
            ]
        );
    },
    'user_get_all_paginated_action' => static function (Container $container): UserGetAllPaginatedApiAction {
        return new UserGetAllPaginatedApiAction(
            $container->get('user_repository'),
            [
                'find_by_criteria' => static function (): array
                {
                    return [];
                },
                'order_by' => static function (): array
                {
                    return ['usr_ref' => 'ASC'];
                },
            ]
        );
    },
    'user_get_all_action' => static function (Container $container): UserGetAllApiAction {
        return new UserGetAllApiAction(
            $container->get('user_repository'),
            [
                'find_by_criteria' => static function (): array
                {
                    return [];
                },
                'order_by' => static function (): array
                {
                    return ['usr_ref' => 'ASC'];
                },
            ]
        );
    },
    'user_get_one_by_id_action' => static function (Container $container): UserGetOneByIdApiAction {
        return new UserGetOneByIdApiAction(
            $container->get('user_repository'),
            [
                'find_one_by_criteria' => static function (string $id): array
                {
                    return ['usr_id' => (int) $id];
                },
            ]
        );
    },
];
