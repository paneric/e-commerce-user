<?php

declare(strict_types=1);

use ECommerce\User\Gateway\UserDTO;

return [

    'validation' => [

        'post.api-usr' => [
            'methods' => ['POST'],
            UserDTO::class => [
                'rules' => [
                    'credential_id' => [
                        'required' => []
                    ], 

                    'ref' => [
                        'required' => []
                    ], 
                    'is_active' => [
                        'required' => []
                    ], 

                ],
            ],
        ],

        'put.api-usr' => [
            'methods' => ['PUT'],
            UserDTO::class => [
                'rules' => [
                    'credential_id' => [
                        'required' => []
                    ], 

                    'ref' => [
                        'required' => []
                    ], 
                    'is_active' => [
                        'required' => []
                    ], 

                ],
            ],
        ],
    ]
];
