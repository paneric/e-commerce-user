<?php

declare(strict_types=1);

use Paneric\ModuleResolver\ModuleMapper;

$moduleMapper = new ModuleMapper();

$moduleMapCrossPaths = [
    'vendor/paneric/e-commerce-credential', 

];

$moduleMapPath = 'vendor/paneric/e-commerce-user';

$moduleMap = [
    'usr'      => ROOT_FOLDER . '%s/src/UserApp',
    'api-usr'  => ROOT_FOLDER . '%s/src/UserApi',
    'api-usrs' => ROOT_FOLDER . '%s/src/UserApi',
    'apc-usr'  => ROOT_FOLDER . '%s/src/UserApc',
];

return [
    'default_route_key' => 'usr',
    'local_map' => ['en', 'pl'],
    'dash_as_slash' => false,
    'merge_module_cross_map' => true,

    'module_map' => $moduleMapper->setModuleMap($moduleMap),

    'module_map_cross' => $moduleMapper->setModuleMapCross(
        $moduleMapCrossPaths,
        $moduleMap,
        $moduleMapPath,
        __DIR__ !== ROOT_FOLDER . 'src/config'
    ),
];
