-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Czas generowania: 06 Sty 2021, 01:58
-- Wersja serwera: 5.7.32-0ubuntu0.18.04.1
-- Wersja PHP: 7.3.25-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `e_commerce`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `my_services`
--

CREATE TABLE `my_services` (
  `ms_id` int(11) UNSIGNED NOT NULL,
  `ms_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ms_pl` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ms_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ms_created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ms_updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `my_services`
--

INSERT INTO `my_services` (`ms_id`, `ms_ref`, `ms_pl`, `ms_en`, `ms_created_at`, `ms_updated_at`) VALUES
(1, 'belgium', 'Belgia', 'Belgium', '2020-12-02 14:13:04', '2021-01-02 05:01:11'),
(2, 'bulgaria', 'Bułgaria', 'Bulgaria', '2020-12-02 14:13:04', '2020-12-02 14:13:58'),
(3, 'croatia', 'Chorwacja', 'Croatia', '2020-12-02 14:13:04', '2020-12-02 14:14:03'),
(4, 'cyprus', 'Cypr', 'Cyprus', '2020-12-02 14:13:04', '2020-12-02 14:14:07'),
(5, 'czech_republic', 'Republika Czeska', 'Chech Republic', '2020-12-02 14:13:04', '2020-12-02 15:28:41'),
(6, 'denmark', 'Dania', 'Denmark', '2020-12-02 14:13:04', '2020-12-02 14:14:16'),
(7, 'estonia', 'Estonia', 'Estonia', '2020-12-02 14:13:04', '2020-12-02 14:14:20'),
(8, 'finland', 'Finlandia', 'Finland', '2020-12-02 14:13:04', '2020-12-02 14:14:25'),
(9, 'france', 'Francja', 'France', '2020-12-02 14:13:04', '2020-12-02 14:14:29'),
(10, 'germany', 'Niemcy', 'Germany', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
(11, 'grece', 'Grecja', 'Grece', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
(12, 'hungary', 'Węgry', 'Hungary', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
(13, 'ireland', 'Irlandia', 'Ireland', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
(14, 'italy', 'Włochy', 'Italy', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
(15, 'latvia', 'Łotwa', 'Latvia', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
(16, 'lithuania', 'Litwa', 'Lithuania', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
(17, 'luxembourg', 'Luksemburg', 'Luxembourg', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
(18, 'malta', 'Malta', 'Malta', '2020-12-02 14:20:39', '2020-12-02 14:20:39'),
(19, 'holland', 'Holandia', 'Holland', '2020-12-02 14:20:39', '2020-12-07 10:49:46'),
(20, 'poland', 'Polska', 'Poland', '2020-12-02 14:23:52', '2020-12-02 14:23:52'),
(21, 'portugal', 'Portugalia', 'Portugal', '2020-12-02 14:23:52', '2020-12-02 14:23:52'),
(22, 'romania', 'Rumunia', 'Romania', '2020-12-02 14:23:52', '2020-12-02 14:23:52'),
(23, 'slovakia', 'Słowacja', 'Slovakia', '2020-12-02 14:23:52', '2020-12-02 14:23:52'),
(24, 'slovenia', 'Słowenia', 'Slovenia', '2020-12-02 14:23:52', '2020-12-02 14:23:52'),
(25, 'spain', 'Hiszpania', 'Spain', '2020-12-02 14:23:52', '2020-12-02 14:23:52'),
(26, 'sweden', 'Szwecja', 'Sweden', '2020-12-02 14:23:52', '2020-12-02 14:23:52');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `my_services`
--
ALTER TABLE `my_services`
  ADD PRIMARY KEY (`ms_id`),
  ADD UNIQUE KEY `los_ref` (`ms_ref`),
  ADD KEY `los_created_at` (`ms_created_at`),
  ADD KEY `los_updated_at` (`ms_updated_at`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `my_services`
--
ALTER TABLE `my_services`
  MODIFY `ms_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
